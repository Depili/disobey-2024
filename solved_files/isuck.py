#!/usr/bin/env python
import sys
import time
import telnetlib

inputs = open(sys.argv[1],"r")
t = telnetlib.Telnet("kouvostopankki.fi",42851)

t.read_until(b"Target address:")
t.write(b"mx.kouvostopankki.fi\n")
time.sleep(0.1)
print(t.read_very_eager())
t.write(b"25\n")
time.sleep(0.1)
print(t.read_very_eager())
for f in inputs.readlines():
    t.write(f.encode("utf-8"))
    time.sleep(0.3)
    print(t.read_very_eager())
t.write(b"\n")
time.sleep(0.3)
print(t.read_very_eager())
t.write(b".\n")
time.sleep(0.3)
print(t.read_very_eager())
t.write(b"QUIT\n")
time.sleep(0.3)
print(t.read_very_eager())
t.write(b"HUPS\n")