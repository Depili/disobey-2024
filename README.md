# Disobey 2024 puzzle

## The only difference between science and screwing around is writing it down

This is my writeup of the 2024 disobey puzzle for the hacker badges. This is the second time I'm solving the puzzle and this time the rabbit hole didn't seem to be as deep. Still took a week. Bear in mind that this is a boiled down description of the events, not included is many periods of being utterly lost, long times of utter frustration and despair.

This year didn't have as much hunting for a needle in a haystack than last year and at least with hindsight the challenge was pretty straight-forward. This also meant that some of the alternative paths for the same knowledge presented in last years challenge were missing this time. There were also way less memes this time around, or at least I didn't find them. The writeup doesn't include the meme content, if you want to find them look for it yourself.

If you want to try your hand at solving the stuff, all of the original relevant files are in the `challenge_files` directory. Here is the minimal context about them:
* ahven.mp4 is the initial announcement video from disobey.fi
* kouvostopankki_deface.html and 4hv3n.js are the files from defaced kouvostopankki.fi site linked in the puzzle announcement
* cta.mp4 is the result of solving 4hv3n.js and doing content discovery
* capture.pcap is the result of solving cta.mp4
  * this is the final step in the puzzle

Beware, there are **SPOILERS** ahead on the rest of the document.

## Tools used

* [Ghidra](https://ghidra-sre.org/)
* [jadx](https://github.com/skylot/jadx)
* [cyberchef](https://gchq.github.io/CyberChef/)
* [ffuf](https://github.com/ffuf/ffuf)

## General flow of things
* kouvostopankki.fi
  * javascript
    * blackblackpinkbrown.4hv3n.fi
      * 4/h/v/3/n/cta.mp4
* cta.mp4
  * Says that credentials to kouvostopankki's ids system backup file server are hidden in it
  * few frames have the username KPP_IDS_ADMIN
  * audio has a weird thing in the spectrogram
    * Password is THIS_PASSWORD_IS_SECURE
  * gives the goal as getting to kouvostopankki intranet
    * quess the url https://intranet.kouvostopankki.fi/ which has a login form shown on the video
* https://KPP_IDS_ADMIN:THIS_PASSWORD_IS_SECURE@kouvostopankki.fi/backup/~operator/ids/
  * capture.pcap file
    * I.S.U.C.K session
    * imap session and credentials, creds don't work
      * Conversation asks for the intranet url
    * http gets for update.sh and uuid file (ransomware.bin)
    * a reverse shell session triggered by the update.sh file
* ransomware.bin and .enc files from the .pcap
  * Reverse engineer with ghidra, create a decryptor
* mfa.apk
  * MFA token endpoint
* I.S.U.C.K
  * Can reach mx.kouvostopankki.fi:25 for sending mail past the SPF....
* mx.kouvostopankki.fi
  * DNS, maybe leftover for puzzle testing? didn't find anything interesting in it
  * imap & smtp
    * the smtp banner is: `220 mx.kouvostopankki.fi ESMTP KouvostoPankki M.T.A - Malicious Transport Agent v0.1`
* Lets go phishing
  * get a Amadea's login creds
* intranet.kouvostopankki.fi
  * Log into the intranet with Amadea's username and password, supply the MFA token as requested
  * get a link to the ticket store

## Initial recon


The puzzle was announced on https://disobey.fi/ with the following text:
```
What’s going on in Kouvostoliitto? A hacker group AHVEN has declared an operation to overthrow the leadership of Kouvostoliitto.

Disobey Hacker Puzzle is live
```

With a link to https://kouvostopankki.fi and a video ahven.mp4

The announcement was later amended with following text:
```
The hacker puzzle does not require bruteforcing any web portals. You can find out all the credentials by solving the challenges. Outside of content discovery, if your brute forcing would be > 1024 tries, it's not the intended solution. Save the (virtual)environment and don't melt the icebergs.
```

Might have contributed to that problem...

Dig into the domain, nmap everything, check crt.sh. This gives us:
* mx.kouvostopankki.fi has a certificate
* kouvostopankki.fi has spf record, txt record that it is disobey puzzle domain
* kouvostopankki.fi open ports:
  * 22 - ssh
  * 80 - http
  * 443 - https
  * 42851 - unknown
* kouvostopankki.fi ffuf with common.txt
  * /backup/~operator/ids with basic auth enabled
* mx.kouvostopankki.fi open ports:
  * 22 - ssh
  * 25 - smtp (this might be filtered by some ISPs)
  * 53 - dns
  * 465 - smtps
  * 587 - submissions (smtp + STARTTLS)
  * 993 - imaps
* defaced kouvostopankki.fi web page
  * loads assets from: 4hv3n.fi
    * open ports
      * 22 - ssh
      * 80 - http
      * 443 - https
    * ffuf with common.js
      * many hits, all of them memes of some kind

## 4hv3n.js

This is the javascript from the defaced kouvostopankki.fi site. I first used a online javascript deobfuscator in string eval mode to get partway there. Then did some string find-replaces and finally used the browser console to get the last remaining function return values by doing the calls.

The deobfuscation gives us instructions to apply for ahven at `http://blackblackpinkbrown.4hv3n.fi`. The domain doesn't have a DNS entry, so probably a normal trick with a virtual host in a web server with the domain missing from dns. This can be gotten around by sending the "Host: blackblackpinkbrown.4hv3n.fi" header or just adding the domain in /etc/hosts on linux or macos.

Index has just a picture of ahven on it, lets ffuf for content discovery. Using common.txt from seclists: `ffuf -w ~/wordlists/common.txt -u http://blackblackpinkbrown.4hv3n.fi/FUZZ` and recursing (need to figure out how to automate it...) we find `http://blackblackpinkbrown.4hv3n.fi/4/h/v/3/n/` and from there we can get a html page with a link to `cta.mp4`

## cta.mp4

A call to arms from Ahven against kouvostoliitto and kouvostopankki. The spoken text promises that the video contains credentials for kouvostopankki's intrusion detection system backup file server.

Playing the video back at the lowest speed possible in VLC reveals few frames with inverted colors, the frames contain `username: KPP_IDS_ADMIN` text and `password:` text, with the rest being blocked by a image on screen...

Figuring out the PW took ages for me, as somehow looking at the audio didn't cross my mind. Before that I extracted every frame of the video with ffmpeg, looked at them with different color profiles etc and didn't find anything. After loading the audio into audacity and flipping the display to spectrogram it was immediately obvious that there was Something there while the dude said "contains the credentials to.."

Fuzzing with the spectrogram settings and thinking about possible encodings for the data it finally became obvious that the spectrogram itself spelled out the password: `THIS_PASSWORD_IS_SECURE`.

Video also talked about getting into the kouvostopankkis intranet as the end goal and had a screenshot of the login page. The first guess led to `https://intranet.kouvostopankki.fi`. It has a login page with username and password fields. The intranet server had very strict rate limits to deal with bruteforcing.

## capture.pcap

With the credentials from the video one could retrieve the `capture.pcap` from `https://kouvostopankki.fi/backup/~operator/ids`. Opening the pcap in wireshark we can see the following in it:
* http downloads from 4hv3n.fi
  * http://4hv3n.fi/update.sh - a reverse shell script
  * http://4hv3n.fi/06003b3c-7124-45bd-9ca2-ca3768d7ac89 - ransomware.bin
* imap session, times 2
  * Credentials for amadea.harjumotto@kouvostopankki.fi : SinCitysColdAndEmpty
    * Credentials don't work
  * A mail chain, where Pinja promises to return with the intranet address to Amadea
* I.S.U.C.K session
* the reverse shell session
  * downloading and running the ransomware
  * base64 encoded encrypted files
    * mfa.apk.enc
    * employees.csv.enc
    * sadevesikouruinsinoori.csv

## ransomware.bin

```
# file ransomware.bin
ransomware.bin: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=397a6859e3a0cc209f8f439592e1e9c0803bae74, for GNU/Linux 4.4.0, stripped
```

This is linux elf executable. It takes in a filename to encrypt and 16 byte key. It then saves the file as .enc after encryption. The ransomware.bin doesn't itself delete any data. It can be ran on ubunty 22.04 jammy jellyfish. I used ghidra only to do the reverse engineering of the binary, mostly relying on just the decompiler output as my x86 assembly knowledge is quite limited, but I have experience of reverse engineering z80 and other older architectures and general ghidra usage.

During analysis I figured out that doing two patches to the binary improves the dissassembly greatly. I did the patches using ghidras bytes view and they are included in the ghidra project linked in this writeup.

* First at `0010179d` patch `05` to `60`
  * This just makes the relative jump be aligned to instructions instead of jumping to the 0x90 byte of the next instructions immediate values last byte, which is also a NOP
* Second at `001014a3` - `001014a9`
  * This is a branch that will never be taken and leads to code that messes up the decompilation a little bit, replacing the bytes with 0x90 improves things.

Otherwise the reverse engineering was pretty straight-forward, some of the traps laid in the code:
* The file contents are copied starting from offset 1 to the read buffer, meaning that the first byte of the ciphertext is Special
  * The first byte stores the amount of padding bytes added to the end
* There is a nice mixing routine that shuffles the bytes around in a buffer, that I reverse engineered and implemented, only to realize that the buffer it operates on gets overridden in the next call...
* The encryption routine has a JMP RAX instruction, where the address is calculated from a return address of a function call and then searching for bytes 0x69 ('i') followed by 0xfe
  * Having instinctively renamed the iterator to i this caused some trouble to my tired brain
  * Figuring out the destination and overriding the jump to `001015dd` makes the encryption loop really nice
    * If needed clear the existing code around that address and dissassemble it again starting from the correct offset
* The initial IV for the encryption comes from the pointer to `main` 0x55, 0x48, 0x89, 0xe5, 0x48, 0x81, 0xec, 0xe0, 0x00, 0x00, 0x00, 0x89, 0xbd, 0x2c, 0xff, 0xff

The encryption works in the following way:
1. Create ouput buffer, size is the next multiple of 16 bytes from the original file size
  * This adds a minimum of 1 byte to the file, and at most 16 bytes.
2. Fill the read buffer with 0x30
3. Store the number of bytes added - 1 into read_buffer[0]
4. Copy the original file to the read buffer starting from offset 1
  * This leaves read_buffer[0] == number of padding bytes at end, and the padding bytes set to 0x30
5. Initialize the key2 to 0x55, 0x48, 0x89, 0xe5, 0x48, 0x81, 0xec, 0xe0, 0x00, 0x00, 0x00, 0x89, 0xbd, 0x2c, 0xff, 0xff
6. Start encrypting 16 byte blocks from read_buffer
7. encrypted := block ^ key ^ key2
8. set key2 = encrypted
9. store encrypted to output buffer
10. repeat from 6 until done
11. write output buffer to file.enc

### Decryptor

#### The hard way, or how I initially did it

I first implemented encryption in go testing it against samples made with encrypting samples with ransomware.bin. I had one sample with data being all nulls and the key being all 'A's. And a second one with all random plaintext and key. Did the initial coding and testing with the constant data, then moved to the random sample and at this point I figured that I don't really need the byte rotation stuff.. oh well.

For figuring out the key I made the intial educated guess that the mfa.apk starts with a zip header, giving us 4 known bytes `0x50 0x4b 0x03 0x04`. Doing a decryption on the first 32 bytes of the file with a null key gives us a stream to XOR with the known values, now we know bytes 1-5 of the key.

Then I made the code decrypt the first 32 bytes from the employees.csv and sadevesikouruinsinööri.txt. Having both results visible from the same run made it easy to just figure out the key byte by byte, as I had 4 locations affected by each key byte at the time. As I kept the unknown key bytes as 0x00 getting the real key byte was just xor:n whatever was at a 'plaintext' location with the desired result.

In the end the key used turned out to be `0xb5, 0x75, 0x41, 0xdb, 0x1d, 0xd0, 0x6c, 0x03, 0x50, 0x16, 0xa2, 0x59, 0xd9, 0x7b, 0x68, 0x7d`. Sadly it seems to be really random instead of a meme, not even the dvd encryption key, so sad :(


#### The Better Way of getting the key

When I was writing this document I also figred out a better way to get the key without doing any guesses about the plaintext. This is possible because we know the padding method and the original plaintext file sizes. From the terminal session in the `capture.pcap` we get:
```
ls -l
total 4356
-rw-r--r-- 1 amadea amadea     224 Sep  7 12:58 employees.csv
-rw-r--r-- 1 amadea amadea 4448140 Sep  7 12:58 mfa.apk
-rw-r--r-- 1 amadea amadea    3984 Sep  7 12:58 sadevesikouruinsinoori.txt
-rwxr-xr-x 1 amadea amadea      55 Sep  7 12:47 update.sh
```

Both the `employees.csv` and `sadevesikouruininoori.txt` files have sizes divisable by 16. This means there will be 16 bytes of padding added to them. So from the algorithm we know the following about the plaintext buffer:
* buffer[0] == 0x0f
* last 15 bytes will be 0x30

This gives us 16 bytes of known plaintext at all offsets of the buffer and with knowledge of the IV we can leak the whole key with, `end` being the last offset of the encrypted file:
* key[0] = 0x0f ^ enc[0] ^ 0x55
* key[1] = 0x30 ^ enc[end-15] ^ enc[end-15-16]
* key[2] = 0x30 ^ enc[end-14] ^ enc[end-14-16]

and so forth. Doing this all in go means that getting the key is just few lines of code:
```go
  fmt.Printf("Getting the key the easy way!\n")

  f, _ = os.ReadFile("sadevesikouruinsinoori.txt.enc")
  easyKey := make([]byte, 16)
  end := len(f)
  easyKey[0] = f[0] ^ 0x0f ^ 0x55
  for i := 1; i < 16; i++ {
    pos := end - 16 + i
    easyKey[i] = 0x30 ^ f[pos] ^ f[pos-16]
  }
  fmt.Printf("%x\n", easyKey)
```

Thats it, leaking the key in 8 lines of code and only knowing the first byte of the IV.

##### Taking it further and limiting initial knowledge

Without any knowledge of the IV we will be missing the first 15 bytes of the real file and then every first byte of each 16 byte block of the file. Bruteforcing the first byte of the IV should be doable given the context of the rest of the file. This would leave us only not knowing the first 15 bytes of the encrypted file. Headers, like the ZIP header in .apk, could then be obvious given the context of the rest of the file and reduce the unknown data even further.

#### Some additional musings about the crypto

The algorithm is also weak in the sense that each second block doesn't depend on the key at all, since:

```
encBlock1 := plainBlock1 ^ key ^ iv
encBlock2 := plainBlock2 ^ key ^ encBlock1 = plainBlock2 ^ key ^ plainBlock1 ^ iv
           = plainBlock2 ^ key ^ key ^ plainBlock1 ^ iv
           = plainBlock2 ^ plainBlock1 ^ iv
encBlock3 := plainBlock3 ^ key ^ encBlock2
           = plainBlock3 ^ key ^ plainBlock2 ^ plainBlock1 ^ iv
encBlock4 := plainBlock4 ^ plainBlock3 ^ plainBlock2 ^ plainBlock1 ^ iv
           = plainBlock4 ^ plainBlock3 ^ encBlock2
```

This means that encrypting anything that has repeating data will leak the IV. Any known bytes from the first 31 bytes can also be used to decrypt bytes from the neighboring block.

## mfa.apk

Used `jadx` to analyze it. In the end this turned out to be quite boring, not even hidden memes in it.

Interesting bits are:
* source code
  * com
    * decryptstringmanager
      * DecryptString
        * contains functions to decrypt the various encrypted strings elsewhere
    * kkp.mfa
      * MainActivity
        * Uses the encrypted strings to obfuscate things

Per DecryptString:
* Generate a key with PBKDF2, SHA1 hash, 128 iterations, key length 256
  * Passphrase: `MQHShVNwFfckR2Bb1qcyCzf8nIZGJ66y`
  * Salt: `MQHShVNwFfckR2Bb1qcyCzf8nIZGJ66y`
* Use the generated key with AES ECB to decrypt stuff with cyberchef
  * Key: `1443ee4459d444c25426de099d2e6e17a89b6409daaa25ba61f9af1e9dbd3235`

Use this to decrypt the strings in:
```js
this.httpClient.newCall(new Request.Builder().url(DecryptString.decryptString("e26f2337f938546138f24e3bfa845718a6a00f492066144d20522ca1179a470fb4785b536a53ae2ac1f9a13f18b47af3") + str).header(DecryptString.decryptString("6a5dd4e112047cec6077c7b74229d33f"), DecryptString.decryptString("0f894d37640f3b12e0515f3089ef19fb9f0c2523d0cf83a18fe5a678e7fe138c266b1e0d4e8d6c121896bcde82ea940c03717a985cbd1f8643db600d50dd7d1e574cdb207abbc0af47e4c7f35d07ad94")).header(DecryptString.decryptString("908c56ad4f3573135bda5ecc634c4f2e"), DecryptString.decryptString("81f9c5cc01c2a1482ca44ba2d4f8a252e7fe6b5123794c391ee363b57091f3c4")).build())
```

So we get:
```js
this.httpClient.newCall(new Request.Builder().url("https://kouvostopankki.fi/get_code?id=" + str).header("Authorization", "Bearer 0e3001c91f1c4617d334eb95ee99f2b78a5b63bfde640571669f2b44f8e7ccb1").header("Host", "intranet.kouvostopankki.fi").build())
```

The parameter is refered as employee_id in the source code.

Combined with the employee id from emploees.csv this translates to:
```bash
curl -H "Authorization: Bearer 0e3001c91f1c4617d334eb95ee99f2b78a5b63bfde640571669f2b44f8e7ccb1" "https://intranet.kouvostopankki.fi/get_code?id=88426"
```

Only Amadea's employee id works, so we are now hunting for her login and password.

## I.S.U.C.K.

```
 _  __                    _         _____    _
| |/ /___ _  ___ _____ __| |_ ___  |_   _|__| |___ __ ___ _ __
| ' </ _ \ || \ V / _ (_-<  _/ _ \   | |/ -_) / -_) _/ _ \ '  \
|_|\_\___/\_,_|\_/\___/__/\__\___/   |_|\___|_\___\__\___/_|_|_|

I.S.U.C.K - Internal Service for Unintentional Crap and Krap v0.1
```

This is the service available at `kouvostopankki.fi:42851`. It is a one-way telnet to given address and port. The service behaves little differently if it can open the connection, echoing the sent data with "Sending data: YOUR_LINE" after it if it can open the connection, no echo if the connection isn't open. This detail tells that the session in the pcap is fake, and didn't connect to the endpoint.

Testing this targeting a host I controlled revealed that it just opens a tcp connection and then sends whatever it is given to that connection. The connection comes from kouvostopankki.fi.

## Phishing for credentials, or how to make sure your logging and tcpdumping filters are sane

The `capture.pcap` contains an interesting e-mail chain, where Pinja promises to return with the intranet address to Amadea. The `kouvostopankki.fi` has a SPF record `v=spf1 a mx -all`. This means we can only send mail from kouvostopankki.fi or mx.kouvostopankki.fi. Lucky for us we have I.S.U.C.K. available. It just gives our oneway connection from `kouvostopankki.fi` and get around the SPF check.

Spent ages in this stage trying different payloads as it felt good direction, but got no results by sending mail via I.S.U.C.K. This turned out to be because:
* My http server that I was using was configured in a silly way, not logging any http requests
* tcpdump being filtered to only kouvostopankki.fi and mx.kouvostopankki.fi

Only after much despair (more than a day) I ended up testing my logging with few curl lines and noticed that I was missing all of the goods...

The payload used was:
```email
EHLO kouvostopankki.fi
MAIL FROM: pinja.pirivirkkala@kouvostopankki.fi
RCPT TO: amadea.harjumotto@kouvostopankki.fi
DATA
To: <amadea.harjumotto@kouvostopankki.fi>
From: <pinja.pirivirkkala@kouvostopankki.fi>
Subject: Re: Spam issues

Dear Amadea,

The intranet is available at address http://not.evil.honest.example/

Warm regards,
Pinja Pirivirkkala
IT Department
Kouvostopankki

.
```

So long story short, just about the first attempts worked and we got connections from `office-dyn-213.kouvostopankki.fi` after sending a phishing mail. Looking at the connections and building the phishing site up it became apparent that all of the static assets also needed be mirrored. Chancing the form method to `GET` makes getting the credentials easier.

After all of this we are rewarded with `GET login?username=amadea.harjumotto%40kouvostopankki.fi&password=hitman_Vic_Le_Big_Mac_020120` victory!

## Intranet

Doing url-decode on the username we get `amadea.harjumotto@kouvostopankki.fi`, use it to login with `hitman_Vic_Le_Big_Mac_020120`. This gives us a form asking for the MFA token, retrieve it with curl and enter it. This redirects us to the ticket store for hacker badges, yay!


## Files

* [4hv3n.js - the first step](challenge_files/4hv3n.js)
* [cta.mp4](challenge_files/cta.mp4)
* [cta_v2.mp4 - they added subtitles to the video at some point](challenge_files/cta_v2.mp4)
* [cta.mp4_audio.aac](solved_files/cta.mp4_audio.aac)
* [cta.mp4_audio_spectrogram.png](solved_files/cta.mp4_audio_spectrogram.png)
* [cta.mp4_audio_spectrogram_settings.png](solved_files/cta.mp4_audio_spectrogram_settings.png)
* [capture.pcap](challenge_files/capture.pcap)
* [ransomware.bin](solved_files/ransomware.bin)
* [mfa.apk.enc](solved_files/mfa.apk.enc)
* [employees.csv.enc](solved_files/employees.csv.enc)
* [sadevesikouruinsinoori.txt.enc](solved_files/sadevesikouruinsinoori.txt.enc)
* [ransomware.gar - ghidra project](solved_files/ransomware.gar)
  * To import close any and all ghidra tools and then close the active project in the main window and then select "File > Restore project..."
* [decryptor.go - ransomware decryptor code, golang, not pretty](solved_files/decryptor.go)
* [mfa.apk](solved_files/mfa.apk)
* [employees.csv](solved_files/employees.csv)
* [sadevesikouruinsinoori.txt](solved_files/sadevesikouruinsinoori.txt)
* [isuck.py - utility to bounce data with isuck](solved_files/isuck.py)

## Other writeups

* https://gofore.com/en/disobey-2024-hacker-puzzle-walkthrough/
* https://github.com/JubinBlack/Disobey24_writeup
