package main

import (
	b64 "encoding/base64"
	"fmt"
	"os"
)

const (
	testExpected = "GwnIpAnAraFBQUHI/G2+vlpIieVIgezgAAAAib0s//8bCcikCcCtoUFBQcj8bb6+WkiJ5UiB7OAAAACJvSz//xs5+JQ58J2RcXFx+Mxdjo4="
	apkStart     = "422DPVFFgOtYHqLrCnXAglYYwuZMlezoCAgAodMOqL4="
	csvStart     = "71uhTCYl7oI9c468BSTj7DtDhbtZnPD1BQFNkblz7vw="
	txtStart     = "72+hTT447oIiYMu/DTnj6yh7kPlRnPboSFY6h7An/fM="
	fileRND      = "kmVxQ01D0kMqVAhuPTmFXg7xVeQksJZJkztbPmeeK5w5eGhlzFTwUD2+KAJRKIhkk+jjHoWda5cOqWaoN+4VKw=="
	keyRND       = "FiEtJLyuR5CHoBvmDDF3cw=="
	encRND       = "TPvBsLdi6KLEik9n3yCxCQTUHcHv6B+kCrlv2u12WFGOzEiNNooMxN0kyhTjFgeq/H6NSpSh1j/NiniURxCezMFvkF4YP6GfehpTQnsR2Y8="
	encA         = "G5ut1UqN7nMCaxXAklCHOwTUHcHv6B+kCrlv2u12WFHZrCToy2UKFRvFkLOuZjGY/H6NSpSh1j/NiniURxCezJYP/Dvl0KdOvPsJ5TZh770="
	keyA         = "QUFBQUFBQUFBQUFBQUFBQQo="
)

var iv = []byte{0x55, 0x48, 0x89, 0xe5, 0x48, 0x81, 0xec, 0xe0, 0x00, 0x00, 0x00, 0x89, 0xbd, 0x2c, 0xff, 0xff}
var nullKey = []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

func main() {
	// realKey := []byte{0x00, 0x6e, 0x09, 0xdc}

	realKey := []byte{0xb5, 0x75, 0x41, 0xdb, 0x1d, 0xd0, 0x6c, 0x03, 0x50, 0x16, 0xa2, 0x59, 0xd9, 0x7b, 0x68, 0x7d}
	fmt.Printf("Realkey known: %d\n", len(realKey))

	padding := make([]byte, 16-len(realKey))
	realKey = append(realKey, padding...)

	fmt.Printf("nullKey len: %d\n", len(nullKey))

	fmt.Printf("realKey len: %d\n", len(realKey))
	fmt.Printf("realkey: %v\n", realKey)

	test := []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}

	fmt.Printf("Input:   %v\n", test)

	rotated := rotateBytes(test)
	fmt.Printf("Rotated: %v\n", rotated)

	undo := undoRotation(rotated)
	fmt.Printf("Undo;    %v\n", undo)

	testKey := []byte("AAAAAAAAAAAAAAAA")
	fmt.Printf("testkey len: %d\n", len(testKey))
	testData := make([]byte, 64)

	encrypted := encrypt(testData, testKey)
	base := b64.StdEncoding.EncodeToString(encrypted)
	fmt.Printf("test encrypted: %v\n", base)
	fmt.Printf("expected:       %v\n", testExpected)

	plain := decrypt(encrypted, testKey)
	fmt.Printf("axd: %v\n", plain)

	plainR, _ := b64.StdEncoding.DecodeString(fileRND)
	keyR, _ := b64.StdEncoding.DecodeString(keyA)
	encR, _ := b64.StdEncoding.DecodeString(encA)

	encrypted = encrypt(plainR, keyR)
	same := true
	for i := range encR {
		if encrypted[i] != encR[i] {
			same = false
			fmt.Printf("Byte %d differs! got: %2x expected: %2x\n", i, encrypted[i], encR[i])
		}
	}
	if !same {
		fmt.Printf("Random test vector encryption failed!\n")
		fmt.Printf("key2  : ")
		for i := 0; i < 16; i++ {
			fmt.Printf("%2x ", iv[i])
		}
		fmt.Printf("\n")

		fmt.Printf("my enc: ")
		for i := 0; i < 16; i++ {
			fmt.Printf("%2x ", encrypted[i]^iv[i])
		}
		fmt.Printf("\n")

		fmt.Printf("vs enc: ")
		for i := 0; i < 16; i++ {
			fmt.Printf("%2x ", encR[i]^iv[i])
		}
		fmt.Printf("\n")
	}

	apk, _ := b64.StdEncoding.DecodeString(apkStart)
	apkDecrypt := decrypt(apk, realKey)
	fmt.Printf("APK decrypt start: ")
	for _, b := range apkDecrypt {
		fmt.Printf("%x ", b)
	}
	fmt.Printf("\n")

	txt, _ := b64.StdEncoding.DecodeString(txtStart)
	txtDecrypt := decrypt(txt, realKey)
	fmt.Printf("txt decrypt start:")
	printOut(txtDecrypt)

	csv, _ := b64.StdEncoding.DecodeString(csvStart)
	csvDecrypt := decrypt(csv, realKey)
	fmt.Printf("csv decrypt start:")
	printOut(csvDecrypt)

	f, _ := os.ReadFile("employees.csv.enc")
	plain = decrypt(f, realKey)
	os.WriteFile("employees.csv", plain, 0666)

	f, _ = os.ReadFile("mfa.apk.enc")
	plain = decrypt(f, realKey)
	os.WriteFile("mfa.apk", plain, 0666)

	f, _ = os.ReadFile("sadevesikouruinsinoori.txt.enc")
	plain = decrypt(f, realKey)
	os.WriteFile("sadevesikouruinsinoori.txt", plain, 0666)

	fmt.Printf("Only found this after doing it by hand...\n")
	fmt.Printf("Getting the key the easy way!\n")
	easyKey := make([]byte, 16)
	end := len(f)
	easyKey[0] = f[0] ^ 0x0f ^ 0x55
	for i := 1; i < 16; i++ {
		pos := end - 16 + i
		easyKey[i] = 0x30 ^ f[pos] ^ f[pos-16]
	}
	fmt.Printf("%x\n", easyKey)
	fmt.Printf("%x\n", realKey)
}

func printOut(b []byte) {
	for i, b := range b {
		if i%4 == 0 {
			fmt.Printf("  ")
		}
		fmt.Printf("%2x ", b)
	}
	fmt.Printf("%s\n", b)
}

func decrypt(enc []byte, key []byte) []byte {
	plain := make([]byte, 0)
	if len(enc)%16 != 0 {
		fmt.Printf("FATAL: encrypted lenght is not divisable by block length!")
		return plain
	}

	data := make([]byte, len(enc))
	copy(data, enc)

	key2 := make([]byte, len(iv))
	copy(key2, iv)

	for i := 0; i < len(data); i += 16 {
		b := encryptBlock(data[i:i+16], key, key2)
		// b = undoRotation(b)
		copy(key2, data[i:i+16])
		plain = append(plain, b...)
	}

	// Remove padding
	padLen := int(plain[0])

	return plain[1 : len(plain)-padLen]
}

func encrypt(plain []byte, key []byte) []byte {
	data := []byte{0x0}
	data = append(data, plain...)

	// Padding
	padLen := 16 - (len(data) % 16)
	padding := make([]byte, padLen)

	for i := range padding {
		padding[i] = 0x30
	}

	data = append(data, padding...)

	// Store padding length in byte 0
	data[0] = byte(padLen)

	key2 := make([]byte, len(iv))
	copy(key2, iv)
	encrypted := make([]byte, 0)

	for i := 0; i < len(data); i += 16 {
		// b := rotateBytes(data[i : i+16])
		b := encryptBlock(data[i:i+16], key, key2)
		copy(key2, b)
		encrypted = append(encrypted, b...)
	}
	return encrypted
}

func rotateBlock(b []byte) []byte {
	ret := b[0:4]

	for i := 1; i < 4; i += 1 {
		start := i * 4
		end := start + 4
		block := b[start:end]

		rot := rotateBlock(block, i)
		ret = append(ret, rot...)
	}
	return ret
}

func rotateInner(block []byte, rotations int) []byte {
	b := make([]byte, len(block))
	copy(b, block)
	for i := 0; i < rotations; i += 1 {
		tmp := b[0]
		for j := 0; j < 3; j += 1 {
			b[j] = b[j+1]
		}
		b[3] = tmp
	}

	return b
}

func encryptBlock(block []byte, key []byte, prevBlock []byte) []byte {
	ret := make([]byte, 16)
	for i := 0; i < 16; i++ {
		ret[i] = key[i] ^ prevBlock[i] ^ block[i]
	}
	return ret
}

func undoRotateBlock(b []byte) []byte {
	ret := make([]byte, 16)

	ret[0] = b[0]
	ret[1] = b[1]
	ret[2] = b[2]
	ret[3] = b[3]

	ret[4] = b[7]
	ret[5] = b[4]
	ret[6] = b[5]
	ret[7] = b[6]

	ret[8] = b[10]
	ret[9] = b[11]
	ret[10] = b[8]
	ret[11] = b[9]

	ret[12] = b[13]
	ret[13] = b[14]
	ret[14] = b[15]
	ret[15] = b[12]

	return ret
}
